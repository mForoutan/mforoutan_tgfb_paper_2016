# README #

This repository contains datasets and scripts to reproduce the results for the paper:
**A Gene Expression Signature to Identify Cancer Cell Lines and Samples with TGFβ-induced EMT** 
