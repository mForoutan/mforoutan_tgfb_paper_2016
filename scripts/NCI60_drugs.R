## This script analyses the NCI60 dug data based on TES,
## first we consider all the cell lines together and then,
## we wok on each of the cancer types seperately.

## we need drug data:
## DTP_NCI60_ZSCORES.csv

## Sum_ES.max_ssgsea_upDown_NCI60_reverseRankForDown_median.txt
## NCI60_Entrez_median.txt
## combat_CI.999_upDown.txt

## we also need utility script:
## my_functions.R

cat("Processing NCI60_drugs.R \n")

#setwd("W:/Rdir/TGFb/NCI60/Affy_HG-U133A_B/")

data.path <- "../data/NCI60"
res.path <- "../output/results"
fig.path <- "../output/figures"

##----------- read drug data

drug<- read.csv(sprintf("%s/DTP_NCI60_ZSCORES.csv",data.path), stringsAsFactors = FALSE, na.strings = "na")
head(drug, 3)
dim(drug)  ## 20862    68
names(drug)
length(unique(drug$Drug.name)) ## 4972

nci_cellTypes<- data.frame(cellLines= colnames(drug)[7:66])
nci_cellTypes$cellLines<- as.character(nci_cellTypes$cellLines)
nci_cellTypes$tissues<- sapply(nci_cellTypes$cellLines, function(x) unlist(strsplit(x, "[.]"))[1])

nci_cellTypes$tissues[nci_cellTypes$tissues =="BR"]<- "Breast"
nci_cellTypes$tissues[nci_cellTypes$tissues =="CNS"]<- "Brain"
nci_cellTypes$tissues[nci_cellTypes$tissues =="CO"]<- "Colon"
nci_cellTypes$tissues[nci_cellTypes$tissues =="LC"]<- "Non-Small Cell Lung"
nci_cellTypes$tissues[nci_cellTypes$tissues =="LE"]<- "Leukemia"
nci_cellTypes$tissues[nci_cellTypes$tissues =="ME"]<- "Melanoma"
nci_cellTypes$tissues[nci_cellTypes$tissues =="OV"]<- "Ovarian"
nci_cellTypes$tissues[nci_cellTypes$tissues =="PR"]<- "Prostate"
nci_cellTypes$tissues[nci_cellTypes$tissues =="RE"]<- "Renal"


##------------ read TES obtained by ssGSEA:

ssgsea_scores<- read.table(sprintf("%s/Sum_ES.diff_ssGSEA_upDown_NCI59_reverseRankForDown_median.txt",res.path), header=T, sep="\t")
ssgsea_scores$cellLines<- row.names((ssgsea_scores))
colnames(ssgsea_scores)[1]<- "ssgsea_scores"

sum(duplicated(drug$SMILES.d))   ##[1] 425
sum(duplicated(drug$PubChem.id))  ##[1] 1360

drugs<- drug[, -c( 2, 3, 4, 5, 6, 67, 68)]


medDr<- t(drugs)
colnames(medDr)<- medDr[1,]   ## colnames are NSC IDs
medDr<- medDr[-1,]

cell_lines<- row.names(medDr) 

##------- the valuse are already -log10(GI50), IF WE WANT TO WORK with positive values:

medDr<- apply(medDr, 2, function(x) -(as.numeric(x)))
row.names(medDr)<- cell_lines
medDr[1:4,1:4]

#outPath<- "W:/Rdir/TGFb/NCI60/Affy_HG-U133A_B/cor_plots/"

##-------- merge scores with drug data:
scores_drugs<- merge(ssgsea_scores, medDr, by.x="cellLines", by.y="row.names")    ## 59 19502

##-------- calculate correlation between TES and log10(GI50) for each drug:

corValues<- data.frame(drugs=colnames(scores_drugs)[3:ncol(scores_drugs)], rho=NA, pval=NA)
for(i in 3:ncol(scores_drugs)){
  p<- as.numeric(cor.test(scores_drugs$ssgsea_scores, scores_drugs[,i], method="spearman", exact = F )[3])
  rho<- as.numeric(cor.test(scores_drugs$ssgsea_scores, scores_drugs[,i], method="spearman", exact = F)[4])
  corValues$pval[i-2]<- p
  corValues$rho[i-2]<- rho
}


corValues$rho <- as.numeric(corValues$rho)
corValues$pval <- as.numeric(corValues$pval)
corValues<- corValues[order(corValues$rho, decreasing=T),]

corValues<- corValues[complete.cases(corValues),]

##-------- plot the relationship between TES and drug response for drugs with the highest correlation values (main figure): 

## Soroor! You should probably add the output file name for this export :-)

for (i in 1:nrow(corValues)){
  if (corValues$rho[i] >0.65 | corValues$rho[i] < -0.70){
    indx<- match(corValues$drugs[i], colnames(scores_drugs))
    p<- as.numeric(cor.test(scores_drugs$ssgsea_scores, scores_drugs[,indx], method="spearman", exact = F )[3])
    rho<- as.numeric(cor.test(scores_drugs$ssgsea_scores, scores_drugs[,indx], method="spearman", exact = F)[4])
    
    png(paste0(fig.path, "/cor_TES_NCI60_drug_NSC_", colnames(scores_drugs)[indx] , ".png"), width=800, height=600)
    par(mar=c(5,5,5,1))
    plot(scores_drugs$ssgsea_scores, scores_drugs[,indx], pch=19, col="darkgreen",
         main=paste0("NSC ", colnames(scores_drugs)[indx],
                     " (p= ", round(as.numeric(p), 6), ", rho= ", round(as.numeric(rho),4), ")"),
         xlab="TES", ylab="log10(GI50)", cex.main=2.5, cex.lab=2, cex.axis=2, cex=1.8)
    dev.off()
  }
}

##-------- merge correlation info. with drug names:

d<- drug[,c("Drug.name", "NSC")]
corValues_drugNames<- merge(d, corValues, by.x= "NSC", by.y="drugs")  ## 19499     4
corValues_drugNames<- corValues_drugNames[order(corValues_drugNames$rho, decreasing=T),]
head(corValues,10)
tail(corValues,10)
head(corValues_drugNames)
# 
# write.table(corValues, "NCI60_drugs_NegGI50_ssGSEA_spCorr_allDrugs_NSC.txt", sep="\t", row.names=F)

##-------- export the output:

# write.table(corValues_drugNames, "NCI60_drugs_NegGI50_ssGSEA_spCorr_allDrugs_NSC_drugName.txt", sep="\t", row.names=F)

##-------- Volcano plot: rho-values vs. p-values (main figure):

nci<- corValues_drugNames

png(sprintf("%s/NCI60_volcano_cor_log10pval_allData_NSC.png", fig.path), height=600, width=800)
par(mar=c(6,6,1,1))
plot(nci$rho, -log10(nci$pval), pch=19, col="red", 
     xlab="Spearman correlation (rho)", ylab="-log10(p-value)",
     cex.lab=2, cex.axis=2)
points(nci$rho[nci$rho<0], -log10(nci$pval[nci$rho<0]), pch=19, col="darkgreen")
dev.off()

##------------------------------------------- waterfall for paper (main figure):

## Soroor! You should probably change the name of the output file to something more sensible :-)

#out_path<- "W:/Rdir/TGFb/NCI60/Affy_HG-U133A_B/bar_plots/"
mycols = colorRampPalette(c( "orange", "white", "blue"))

topCLs<- c("150412", "674493")
topCLdrugs<- medDr[, colnames(medDr) %in% topCLs]

cellLineScore<- merge(ssgsea_scores, topCLdrugs, by.x="cellLines", by.y="row.names")
cellLineScore<- cellLineScore[order(cellLineScore$ssgsea_scores, decreasing=T), ]

## note that the NCI60 drug data have been already transformed to Z-scores

png(paste0(fig.path, "/tes_log10GI50_zScore_NSC_150412_674493_col.png"), height=800, width=1800)
#pdf(paste0(fig.path, "/tes_log10GI50_zScore_NSC_150412_674493_col.pdf"), height=10, width=18, useDingbats = F)
par(mfrow=c(1, 3))
par(mar=c(8,2,6,1))
barplot(cellLineScore$ssgsea_scores, horiz = T, cex.axis=2,  #las=2,
        main="NCI60", cex.main=3, col= mycols(length(cellLineScore$ssgsea_scores)), xlab= "TES", cex.lab=2, xaxt="n")
axis(1, hadj=, cex.axis=2)

par(mar=c(8,10,6,1))  
barplot(cellLineScore[,"150412"], horiz = T, names.arg=cellLineScore$cellLines, cex.lab=2, cex.names=1.3, las=2,
        main="NSC 150412", cex.main=3, cex.axis=2, xlab= "log10(GI50)", cex.lab=2, xaxt="n")
axis(1, hadj=1, cex.axis=2)

barplot(cellLineScore[,"674493"], horiz = T, #las=2,
        main="NSC 674493", cex.main=3, cex.axis=2, xlab= "log10(GI50)", cex.lab=2,  xaxt="n")
axis(1, hadj=1, cex.axis=2)

dev.off()


##================================================ Volcano plots for each cancer type (Additional files):

data<- read.table(sprintf("%s/NCI60_Entrez_median.txt", res.path), sep="\t", header=TRUE)

m<- as.matrix(data[,2:ncol(data)])
row.names(m)<- data[,"EntrezID"]
head(m)

##----- Read in the signature:
sig<- read.table(sprintf("%s/combat_CI.999_upDown.txt", res.path), header=T, sep="\t")
head(sig)

up<- sig$EntrezID[sig$upDown == "up"]
down<- sig$EntrezID[sig$upDown== "down"]


##------------------ Generating volcano plots for each of the cancer types seperately (Additional file):

## Soroor! You should probably change the name of the output file to something more sensible :-)

#out_Path<- "W:/Rdir/TGFb/NCI60/Affy_HG-U133A_B/cancer_specific/"
source("./utility/my_functions.R")

uniques<- unique(nci_cellTypes$tissues)

for (i in 1:length(uniques)){
  
  ##-------- Obtaining TES using ssGSEA method 
  
  group<- nci_cellTypes$cellLines[nci_cellTypes$tissues== uniques[i]]
  mat<- m[, colnames(m) %in% group]
  ssgsea_scores<- my_ssGSEA_array (expression=mat, up=up, down=down, method="ssGSEA")
  ssgsea_scores<- as.data.frame(ssgsea_scores)
  ssgsea_scores$cellLines<- row.names((ssgsea_scores))
  colnames(ssgsea_scores)[1]<- "ssgsea_scores"
  medDr<- t(drugs)
  colnames(medDr)<- medDr[1,]   ## colnames are NSC IDs
  medDr<- medDr[-1,]
  
  cell_lines<- row.names(medDr)
  
  ##------- the valuse are already -log10(GI50), IF WE WANT TO WORK with positive values:
  
  medDr<- apply(medDr, 2, function(x) -(as.numeric(x)))
  row.names(medDr)<- cell_lines
  scores_drugs<- merge(ssgsea_scores, medDr, by.x="cellLines", by.y="row.names")    ## 59 19502
  
  corValues<- data.frame(drugs=colnames(scores_drugs)[3:ncol(scores_drugs)], rho=NA, pval=NA)
  
  ##------ correlation between TES and drug response:
  
  for(j in 3:ncol(scores_drugs)){
    if(sum(is.na(scores_drugs[,j])) == 0){
      p<- as.numeric(cor.test(scores_drugs$ssgsea_scores, scores_drugs[,j], method="spearman" )[3])
      rho<- as.numeric(cor.test(scores_drugs$ssgsea_scores, scores_drugs[,j], method="spearman")[4])
      corValues$pval[j-2]<- p
      corValues$rho[j-2]<- rho
      
    }
  }
  corValues$rho <- as.numeric(corValues$rho)
  corValues$pval <- as.numeric(corValues$pval)
  corValues<- corValues[order(corValues$rho, decreasing=T),]
  corValues<- corValues[complete.cases(corValues),]
  
  d<- drug[,c("Drug.name", "NSC")]
  corValues_drugNames<- merge(d, corValues, by.x= "NSC", by.y="drugs")  ## 19499     4
  corValues_drugNames<- corValues_drugNames[order(corValues_drugNames$rho, decreasing=T),]
  
  ##------ if we want to have the output for each of the cancer types:
  
  # write.table(corValues_drugNames, paste0(out_Path, "NCI60_", uniques[i], "_drugs_NegGI50_ssGSEA_spCorr_allDrugs_NSC_drugName.txt"), sep="\t", row.names=F)
  
  ##------ volcano plots:
  
  nci<- corValues_drugNames
  
  png(paste0(fig.path, "/NCI60_", uniques[i], "volcano_cor_log10pval_allData_NSC.png"), height=600, width=800)
  par(mar=c(6,6,4,1))
  plot(nci$rho, -log10(nci$pval), pch=19, col="red", main= paste0(uniques[i], " (n= ", ncol(mat), ")"), 
       xlab="Spearman correlation (rho)", ylab="-log10(p-value)",
       cex.lab=2, cex.axis=2, cex.main=2)
  points(nci$rho[nci$rho<0], -log10(nci$pval[nci$rho<0]), pch=19, col="darkgreen")
  dev.off()
}

cat("End of the script. cleaning up working directory... \n")
rm(list = ls())
